/*
	@file:
		browserSync.js

	@description: 
		Syncs the browser / triggers a refresh on
		any source file change
*/

// Variables
var browserSync             = require('browser-sync');
var gulp                    = require('gulp');
var config                  = require('../config').docsBrowserSync;
var docsbuild               = require('./docsBuild');

// Task
gulp.task('docsBrowserSync',
    gulp.series('docsBuild', function() {
  
	// Trigger sync based on the config object
	browserSync(config);

}));
