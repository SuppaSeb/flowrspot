/*
	@file:
		browserSync.js

	@description: 
		Syncs the browser / triggers a refresh on
		any source file change
*/

// Variables
var browserSync = require('browser-sync');
var gulp        = require('gulp');
var config      = require('../config').browserSync;
var build       = require('./build');

// Task
gulp.task('browserSync',
    gulp.series('build', function(done) {
  
	// Trigger sync based on the config object
	browserSync(config);
	
	done();

}));
