/*
	@file:
		deploy-drupal.js

	@description: 
		Gulp task that does the deployment processing
*/

// Variables
var gulp            = require('gulp');
var sftp            = require('../util/sftpUpload');
var config          = require('../config').deploy;
var php             = require('./php');

gulp.task('deploy-php',
    gulp.series('clean', 'php', function () {

    return gulp.src(config.src)
        .pipe(sftp(config.options));

}));
