/*
	@file:
		sass.js

	@description: 
		Gulp task that does the SASS/SCSS processing
*/

// Variables
var gulp         = require('gulp');
var browserSync  = require('browser-sync');
var sass         = require('gulp-sass');
var sourcemaps   = require('gulp-sourcemaps');
var handleErrors = require('../util/handleErrors');
var config       = require('../config').docsSass;
var autoprefixer = require('gulp-autoprefixer');

// Task
gulp.task('docsSass', function () {

	console.log(config);

	return gulp.src(config.src)
		.pipe(sourcemaps.init())
		.pipe(sass(config.settings))
		.on('error', handleErrors)
		.pipe(sourcemaps.write())
		.pipe(autoprefixer({ browsers: ['last 2 version'] }))
		.pipe(gulp.dest(config.dest))
		.pipe(browserSync.reload({stream:true}));
    
});
