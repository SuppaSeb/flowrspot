/*
	@file:
		deploy-drupal.js

	@description: 
		Gulp task that does the deployment processing
*/

// Variables
var gulp            = require('gulp');
var sftp            = require('../util/sftpUpload');
var config          = require('../config').deploy;

gulp.task('deploy-images',
    gulp.series('clean', 'images', function () {

    return gulp.src(config.src)
        .pipe(sftp(config.options));

}));
