/*
	@file:
		fonts.js

	@description: 
		File that handles the font-face processing.
*/

// Variables
var gulp       = require('gulp');
var config     = require('../config').fonts;

gulp.task('fonts', function() {
	return gulp.src(config.src).pipe(gulp.dest(config.dest));
});