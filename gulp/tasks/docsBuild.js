/*
	@file:
		build.js

	@description: 
		Build task that includes 4 seperate processes
*/

// Variables
var gulp                = require('gulp');
var docsbrowserify      = require('./docsBrowserify');
var docssass            = require('./docsSass');
var docsimages          = require('./docsImages');
var docsmarkup          = require('./docsMarkup');

// Task
gulp.task('docsBuild',
    gulp.series(
        'docsBrowserify',
        'docsSass',
        'docsImages',
        'docsMarkup'
    ));
