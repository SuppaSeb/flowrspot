/*
	@file:
		markup.js

	@description: 
		Gulp task that does the HTML processing
*/

// Variables
var gulp        = require('gulp');
var handlebars  = require('gulp-hb');
var browserSync = require('browser-sync');
var rename      = require('gulp-rename');
var config      = require('../config').markup;

// Task
gulp.task('markup', function () {

	// Define options
    options = {

        ignorePartials: true,
        batch : [ config.partials ]

    };

    return gulp.src( config.src )
        .pipe(handlebars({
            data: config.data,
            helpers: config.helpers,
            partials: config.partials
        }))
        .pipe(gulp.dest( config.dest ))
        .pipe(browserSync.reload({stream:true}));

});
