/*
	@file:
		build.js

	@description: 
		Build task that includes 4 seperate processes
*/

// Variables
var gulp        = require('gulp');
var browserify  = require('./browserify');
var sass        = require('./sass');
var images      = require('./images');
var markup      = require('./markup');

// Task
gulp.task('build',
    gulp.parallel(
        'browserify',
        'sass',
        'images',
        'markup'
    ));
