/*
	@file:
		deploy-drupal.js

	@description:
		Gulp task that does the deployment processing
*/

// Variables
var gulp            = require('gulp');
var sftp            = require('../util/sftpUpload');
var config          = require('../config').deployJs;

gulp.task('deploy-js',
    gulp.series('browserify', function () {

    return gulp.src(config.src)
        .pipe(sftp(config.options));

}));
