/*
	@file:
		deploy-drupal.js

	@description: 
		Gulp task that does the deployment processing
*/

// Variables
var gulp            = require('gulp');
var sftp            = require('../util/sftpUpload');
var config          = require('../config').deploy;
var drupal          = require('./drupal');

gulp.task('deploy-drupal',
    gulp.series('drupal', function () {

    return gulp.src(config.src)
        .pipe(sftp(config.options));

}));
