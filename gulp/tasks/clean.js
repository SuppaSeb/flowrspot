/*
	@file:
		clean-drupal.js

	@description: 
		Gulp task that does the cleaning processing
*/

// Variables
var gulp   = require('gulp');
var del    = require('del');
var config = require('../config').drupal;

gulp.task('clean', function(cb) {

    del(['./build/**/*'], cb);

});