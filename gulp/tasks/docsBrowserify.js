/*
  @file:
    browserify.js

  @description: 
     Bundle javascripty things with browserify!

     This task is set up to generate multiple separate bundles, from
     different sources, and to use Watchify when run from the default task.

     See browserify.bundleConfigs in gulp/config.js

*/

// Variables
var browserify   = require('browserify');
var watchify     = require('watchify');
var bundleLogger = require('../util/bundleLogger');
var gulp         = require('gulp');
var handleErrors = require('../util/handleErrors');
var source       = require('vinyl-source-stream');
var config       = require('../config').docsBrowserify;
var uglify       = require('gulp-uglify');
var streamify    = require('gulp-streamify');
var jshint       = require('gulp-jshint');
var rename       = require('gulp-rename');

// Task
gulp.task('docsBrowserify', function(callback) {

  // Get all the tasks from the config object
  var bundleQueue = config.bundleConfigs.length;

  // Time for magic
  var browserifyThis = function(bundleConfig) {

    // Bundler
    var bundler = browserify({

      // Required watchify args
      cache: {}, packageCache: {}, fullPaths: true,
      // Specify the entry point of your app
      entries: bundleConfig.entries,
      // Add file extentions to make optional in your requires
      extensions: config.extensions,
      // Enable source maps!
      debug: config.debug

    });

    // The actual bundle of the files
    var bundle = function() {

      // Log when bundling starts
      bundleLogger.start(bundleConfig.outputName);

      // Return the object
      return bundler
        .bundle()
        // Report compile errors
        .on('error', handleErrors)
        // Use vinyl-source-stream to make the
        // stream gulp compatible. Specifiy the
        // desired output filename here.
        .pipe(source(bundleConfig.outputName))
        // Run it through jshint for optimal output
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(streamify(uglify())) // Uglify
        // Specify the output destination
        .pipe(gulp.dest(bundleConfig.dest))
        .on('end', reportFinished);

    };

    // Check for the watch global
    if(global.isWatching) {

      // Wrap with watchify and rebundle on changes
      bundler = watchify(bundler);
      // Rebundle on update
      bundler.on('update', bundle);

    }

    // Callback function
    var reportFinished = function() {

      // Log when bundling completes
      bundleLogger.end(bundleConfig.outputName);

      // Do we still have tasks to be served?
      if(bundleQueue) {

        // Decrease number of tasks
        bundleQueue--;

        if(bundleQueue === 0) {

          // If queue is empty, tell gulp the task is complete.
          // https://github.com/gulpjs/gulp/blob/master/docs/API.md#accept-a-callback
          callback();

        }

      }

    };

    return bundle();

  };

  // Start bundling with Browserify for each bundleConfig specified
  config.bundleConfigs.forEach(browserifyThis);

});