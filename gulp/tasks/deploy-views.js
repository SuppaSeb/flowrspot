/*
 @file:
 deploy-drupal.js

 @description:
 Gulp task that does the deployment processing
 */

// Variables
var gulp            = require('gulp');
var sftp            = require('../util/sftpUpload');
var config          = require('../config').deployViews;
var views           = require('./views');

gulp.task('deploy-views',
    gulp.series('views', function () {

    return gulp.src(config.src)
        .pipe(sftp(config.options));

}));
