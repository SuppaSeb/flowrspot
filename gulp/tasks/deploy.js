/*
	@file:
		deploy-build.js

	@description: 
		Gulp task that does the deployment processing
*/

// Variables
var gulp            = require('gulp');
var sftp            = require('../util/sftpUpload');
var config          = require('../config').deploy;

gulp.task('deploy',
    gulp.series('build', function () {

    return gulp.src(config.src)
        .pipe(sftp(config.options));

}));
