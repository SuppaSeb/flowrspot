/*
	@file:
		drupal.js

	@description: 
		Gulp task that does the Drupal theme processing
*/

// Variables
var gulp        = require('gulp');
var rename      = require('gulp-rename');
var rimraf      = require('gulp-rimraf');
var config      = require('../config').php;

// Task
gulp.task('php', function () {

    return gulp.src( config.src )
        .pipe(gulp.dest( config.dest ));

});
