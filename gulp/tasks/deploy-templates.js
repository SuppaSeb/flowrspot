/*
	@file:
		deploy-drupal.js

	@description: 
		Gulp task that does the deployment processing
*/

// Variables
var gulp            = require('gulp');
var sftp            = require('../util/sftpUpload');
var config          = require('../config').deployTemplates;
var templates       = require('./templates');

gulp.task('deploy-templates',
    gulp.series('templates', function () {

    return gulp.src(config.src)
        .pipe(sftp(config.options));

}));
