/*
	@file:
		images.js

	@description: 
		File that handles the image processing, minifycation and export.
*/

// Variables
var changed    = require('gulp-changed');
var gulp       = require('gulp');
var imagemin   = require('gulp-imagemin');
var config     = require('../config').images;

gulp.task('images', function() {
	return gulp.src(config.src)
	.pipe(changed(config.dest)) // Ignore unchanged files
	.pipe(imagemin({
		progressive: true,
		svgoPlugins: [
			{ removeViewBox: true },               // Don't remove the viewbox atribute from the SVG
			{ removeUselessStrokeAndFill: true },  // Don't remove Useless Strokes and Fills
			{ removeEmptyAttrs: true }
		]
	})) // Optimize
	.pipe(gulp.dest(config.dest));
});