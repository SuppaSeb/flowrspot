/*
	@file:
		setWatch.js

	@description: 
		Async task to define the watch global
*/

// Variables
var gulp = require('gulp');

// Task
gulp.task('setWatch', function(done) {

	// Define global variable
	global.isWatching = true;
    done();
});
