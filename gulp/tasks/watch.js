/*
	@file:
		watch.js

	@description: 
		gulp/tasks/browserify.js handles js recompiling with watchify
		gulp/tasks/browserSync.js watches and reloads compiled files
*/

// Variables
var gulp                = require('gulp');
var browserSync         = require('browser-sync');
var config              = require('../config');
var setwatch            = require('./setWatch');
var browsersync         = require('./browserSync');
var Undertaker          = require('undertaker');

// Task
gulp.task('watch',
    gulp.parallel('setWatch', 'browserSync', function(done) {

    // Init watch for specific tasks
    gulp.watch(config.sass.src,   gulp.series('sass', reload));
    gulp.watch('src/img/svg/*.svg', gulp.series('svgStore', reload));
    gulp.watch(config.images.src, gulp.series('images', reload));
    gulp.watch(config.markup.src, gulp.series('markup', reload));
    gulp.watch(config.markup.partials, gulp.series('markup', reload));
    
    done();
}));

function reload(done) {
    browserSync.reload();
    done();
}
