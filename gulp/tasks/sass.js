/*
	@file:
		sass.js

	@description: 
		Gulp task that does the SASS/SCSS processing
*/

// Variables
var gulp                = require('gulp');
var browserSync         = require('browser-sync');
var sass                = require('gulp-sass');
var sourcemaps          = require('gulp-sourcemaps');
var handleErrors        = require('../util/handleErrors');
var config              = require('../config').sass;
var autoprefixer        = require('gulp-autoprefixer');
var stripCssComments    = require('gulp-strip-css-comments');
var cleanCSS            = require('gulp-clean-css');

// Task
gulp.task('sass', function (done) {

    return gulp.src(config.src)
        .pipe(sourcemaps.init())
        .pipe(sass(config.settings))
        .on('error', handleErrors)
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({ browsers: ['last 2 version'] }))
        .pipe(stripCssComments({preserve: false}))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(config.dest))
        .pipe(browserSync.reload({stream:true}));
        done();
    
});
