/*
	@file:
		drupal.js

	@description: 
		Gulp task that does the Drupal theme processing
*/

// Variables
var gulp        = require('gulp');
var rename      = require('gulp-rename');
var rimraf      = require('gulp-rimraf');
var config      = require('../config').views;

// Task
gulp.task('views', function () {

	console.log(config.src);
	console.log(config.dest);

    return gulp.src( config.src )
        .pipe(gulp.dest( config.dest ));

});
