/*
	@file:
		watch.js

	@description: 
		gulp/tasks/browserify.js handles js recompiling with watchify
		gulp/tasks/browserSync.js watches and reloads compiled files
*/

// Variables
var gulp                = require('gulp');
var browserSync         = require('browser-sync');
var config              = require('../config');
var docsbrowsersync     = require('./docsBrowserSync');

// Task
gulp.task('docs',
    gulp.series('setWatch', 'docsBrowserSync', function() {

	// Init watch for specific tasks
	gulp.watch(config.docsSass.src,   ['docsSass', browserSync.reload]);
	gulp.watch(config.docsImages.src, ['docsImages', browserSync.reload]);
	gulp.watch(config.docsMarkup.src, ['docsMarkup', browserSync.reload]);
	gulp.watch(config.docsMarkup.partials, ['docsMarkup', browserSync.reload]);


}));
