var gulp            = require('gulp');
var svgstore        = require('gulp-svgstore');
var svgmin          = require('gulp-svgmin');
var path            = require('path');
var rename          = require('gulp-rename');
// var config          = require('../config').svgStore;

gulp.task('svgStore', (function () {
    return gulp
        .src('src/img/svg/*.svg')
        // .pipe(svgmin())
        .pipe(svgmin(function (file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore())
        .pipe(rename({basename: 'svg_sprite'}))
        .pipe(gulp.dest('build/img'));
}));
