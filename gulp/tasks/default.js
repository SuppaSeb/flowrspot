/*
	@file:
		default.js

	@description: 
		Default Gulp task that includes the watch process
*/

// Variables
var gulp        = require( 'gulp' );
var watch       = require('./watch');

// Task
gulp.task( 'default',
    gulp.series('watch'));
