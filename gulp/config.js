/*
	@file:
		config.js

	@description:
		Includes all the necessary configs needed
		for the individual Gulp tasks

	@parameter {String} src:
		Relative path for the source folder

	@parameter {String} dest:
		Relative path for the destination folder

*/

// Variables
var src  = './src';
var dest = './build';
var docsSrc = './docs/src';
var docsDest = './docs/build';
var hostname    = 'serverHostname';
var user        = 'username';
var pass        = 'pass';
var port        = 'port';
var remotePath  = '/var/www/lab/path/to/folder/';

// Export the config as a module for later use
module.exports = {
    
    // DOCS BrowserSync config
    docsBrowserSync: {
        
        server: {
            // We're serving the src folder as well
            // for sass sourcemap linking
            baseDir: [docsDest, docsSrc]
        },
        
        files: [
            docsDest + "/css/*",
            docsDest + "/js/*",
            docsDest + "*.html",
            // Exclude Map files
            "!" + docsDest + "/css/*.map"
        ]
        
    },
    
    // BrowserSync config
    browserSync: {
        
        server: {
            // We're serving the src folder as well
            // for sass sourcemap linking
            baseDir: [dest, src]
        },
        
        files: [
            dest + "/css/*",
            dest + "/js/*",
            dest + "*.html",
            // Exclude Map files
            "!" + dest + "/css/*.map"
        ]
        
    },
    
    // SASS config
    sass: {
        
        src: src + "/sass/**/*.scss",
        dest: dest + "/css"
        
    },
    
    // DOC SASS config
    docsSass: {
        
        src: docsSrc + "/sass/**/*.scss",
        dest: docsDest + "/css"
        
    },
    
    // Font-face config
    fonts: {
        
        src: src + "/fonts/*",
        dest: dest + "/fonts"
        
    },
    
    // DOCS Images
    docsImages: {
        
        src: docsSrc + "/img/**/*",
        dest: docsDest + "/img"
        
    },
    
    // Images
    images: {
        
        src: src + "/img/**/*",
        dest: dest + "/img"
        
    },
    
    // Drupal Theme
    drupal: {
        
        src: src + "/drupal/**/*",
        dest: dest
        
    },
    
    // Deploy
    deploy: {
        
        src: dest + '/**/*',
        options: {
            host       : hostname,
            user       : user,
            pass       : pass,
            port       : port,
            remotePath : remotePath
        }
        
    },
    
    
    // Deploy
    deployTemplates: {
        
        src: dest + '/**/templates/*',
        options: {
            host       : hostname,
            user       : user,
            pass       : pass,
            port       : port,
            remotePath : remotePath
        }
        
    },
    
    // Deploy
    deployViews: {
        
        src: dest + '/**/templates/views/*',
        options: {
            host       : hostname,
            user       : user,
            pass       : pass,
            port       : port,
            remotePath : remotePath
        }
        
    },
    
    views: {
        src: src + "/drupal/templates/views/*",
        dest: dest + "/templates/views/"
    },
    
    // Deploy
    deploySass: {
        
        src: dest + '/**/css/*',
        options: {
            host       : hostname,
            user       : user,
            pass       : pass,
            port       : port,
            remotePath : remotePath
        }
        
    },
    
    templates: {
        src: src + "/drupal/templates/*",
        dest: dest + "/templates/"
    },
    
    php: {
        src: src + "/drupal/*",
        dest: dest + "/"
    },
    
    // Deploy
    deployJs: {
        
        src: dest + '/**/js/*',
        options: {
            host       : hostname,
            user       : user,
            pass       : pass,
            port       : port,
            remotePath : remotePath
        }
        
    },
    
    // DOCS Markup config
    docsMarkup: {
        
        src: docsSrc + "/html/*.html",
        partials: docsSrc + "/html/partials/*.hbs",
        data: docsSrc + "/html/data/*.{js,json}",
        helpers: [
            
            docsSrc + "/js/helpers/*.js"
        
        ],
        dest: docsDest
        
    },
    
    // Markup config
    markup: {
        
        src: src + "/html/*.html",
        partials: src + "/html/partials/*.hbs",
        data: src + "/html/data/*.{js,json}",
        helpers: [
            
            src + "/js/helpers/*.js"
        
        ],
        dest: dest
        
    },
    
    // DOCS Browserify config
    docsBrowserify: {
        
        // Enable source maps
        debug: true,
        
        // Additional file extentions to make optional
        extensions: ['.hbs'],
        
        // A separate bundle will be generated for each
        // bundle config in the list below
        bundleConfigs: [{
            entries: './src/js/app.js',
            dest: docsDest + '/js',
            outputName: 'app.js'
        }, {
            entries: './src/js/head.js',
            dest: docsDest + '/js',
            outputName: 'head.js'
        }]
        
    },
    
    // Browserify config
    browserify: {
        
        // Enable source maps
        debug: true,
        
        // Additional file extentions to make optional
        extensions: ['.hbs'],
        
        // A separate bundle will be generated for each
        // bundle config in the list below
        bundleConfigs: [{
            entries: './src/js/app.js',
            dest: dest + '/js',
            outputName: 'app.js'
        }, {
            entries: './src/js/head.js',
            dest: dest + '/js',
            outputName: 'head.js'
        }]
        
    }
    
};
