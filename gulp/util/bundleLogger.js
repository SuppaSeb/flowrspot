/*
  @file:
    bundleLogger.js

  @description: 
    Provides gulp style logs to the bundle method in browserify.js
*/

// Variables
var gutil        = require('gulp-util');
var prettyHrtime = require('pretty-hrtime');
var startTime;

// Export as module for later use
module.exports = {

  // Log in the start of the task in some fancy style
  start: function( filepath ) {

    // Define start time
    startTime = process.hrtime();
    
    // Log it into Gulp
    gutil.log('Bundling', gutil.colors.green(filepath) + '...');

  },

  // Log in the end of task with some fancy style
  end: function( filepath ) {

    // Define task time
    var taskTime = process.hrtime(startTime);
    var prettyTime = prettyHrtime(taskTime);

    // Log the end of task into Gulp
    gutil.log('Bundled', gutil.colors.green(filepath), 'in', gutil.colors.magenta(prettyTime));

  }

};