/*
  @file:
    handleErrors.js

  @description: 
    Handle gulp errors and output them without letting gulp hang on in between
*/

// Get the notify module
var notify = require("gulp-notify");

// Export as module for later use
module.exports = function() {

	var args = Array.prototype.slice.call(arguments);

	// Send error to notification center with gulp-notify
	notify.onError({

		title: "Compile Error",
		message: "<%= error.message %>"

	}).apply(this, args);

	// Keep gulp from hanging on this task
	this.emit('end');
};