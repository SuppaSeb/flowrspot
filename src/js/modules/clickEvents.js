/*
 @file:
 clickEvents.js
 
 @description:
 Core Javascript file that handles most
 of the clicking stuff that happens on the site
 */

// Variables

var $           = require('jquery');

var bodyClass   = require('./bodyClass');
var listeners   = require('./listeners');

$(document).ready(function() {
    
    $('.userAcc').on('click', function(){
        
        $('body').addClass('profile--active');
        
    });
    
    $('#profileClose').on('click', function(){
        
        $('body').removeClass('profile--active');
        
    });
    
    listeners.add('#hamburger', 'click', function () {
        bodyClass.toggle('sideMenu--open')
    });

});
