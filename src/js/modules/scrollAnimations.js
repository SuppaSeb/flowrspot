/*
    @file:
        scrollAnimations.js

    @description:
        Javascript file that handles
        scroll magic on page
*/

// Variables
var $           = require('jquery');

var TweenMax    = require('gsap');
var ScrollMagic = require('scrollmagic');
require('scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap');

$(document).ready(function() {
    
    TweenLite.defaultOverwrite = false;
    
    var controller = new ScrollMagic.Controller({
        globalSceneOptions: {
            // triggerHook: 0.5,
            // reverse: false
        }
    });
    
    if($('#flowers').length) {
    
        var flowerB       = $('#flowers .flower__single');
    
        flowerB.each(function(index, elem){
        
            var flowerBTimeline = new TimelineMax()
        
            flowerBTimeline
                .staggerTo(flowerB, 0.650,
                    {
                        top: 0,
                        opacity: 1,
                        ease: Power1.easeOut
                    }, 0.2)
            //  build a scene
            var scene = new ScrollMagic.Scene({
                triggerElement: elem,
                triggerHook:'onEnter',
                offset: 200,
                reverse: false,
                // duration: 0,
                tweenChanges: true
            })
                .setTween(flowerBTimeline)
                .addTo(controller)
        
        });
        
    }
    
    if($('#cards').length) {
        
        var cardB       = $('#cards .card__single');
    
        cardB.each(function(index, elem){
            
            var cardBTimeline = new TimelineMax()
    
            cardBTimeline
                .staggerTo(cardB, 0.650,
                    {
                        top: 0,
                        opacity: 1,
                        ease: Power1.easeOut
                    }, 0.2)
            //  build a scene
            var scene = new ScrollMagic.Scene({
                triggerElement: elem,
                triggerHook:'onEnter',
                offset: 100,
                reverse: false,
                // duration: 0,
                tweenChanges: true
            })
                .setTween(cardBTimeline)
                .addTo(controller)
            
        });
        
    }
    
    if($('.hero--flower__mainImg').length) {
        
        var mainImg = $('.hero--flower__mainImg');
    
        var mainImgT = new TimelineMax()
    
        mainImgT
            .add(TweenMax.fromTo(mainImg, 0.650,
                {
                    yPercent: -50,
                    opacity: 0
                },
                {
                    yPercent: 0,
                    opacity: 1,
                    ease: Power1.easeOut
                }, 0.2)
            )
        
    }

});
