/*
 @file:
 bodyClass.js
 
 @description:
 Custom module for adding classes to body
 */

var $       = require('jquery');

// Export module
module.exports = {
    
    // Some simple function
    add : function ( string ) {
        if (Array.isArray(string)) {
            for (var i = 0; i < string.length; i++) {
                $('body').addClass(string[i]);
            }
        } else {
            $('body').addClass(string);
        }
    },
    
    toggle: function (string) {
        if (Array.isArray(string)) {
            for (var i = 0; i < string.length; i++) {
                $('body').toggleClass(string[i]);
            }
        } else {
            $('body').toggleClass(string);
        }
    },
    
    remove: function (string) {
        if (Array.isArray(string)) {
            for (var i = 0; i < string.length; i++) {
                $('body').removeClass(string[i]);
            }
        } else {
            $('body').removeClass(string);
        }
    }
    
};
