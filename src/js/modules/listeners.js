/*
 @file:
 listeners.js
 
 @description:
 Custom module for the menu
 */

var $ = require('jquery');

// Export module
module.exports = {
    
    // Some simple function
    add : function ( selector, event, handler ) {
        
        $(selector).on(event, handler);
        
    }
    
};