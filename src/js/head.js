// Javascript file
/*
 @file:
 app.js
 
 @description:
 Core Javascript file that handles most
 of the stuff that happens on the site
 */

// Variables

var $                   = require('jquery');
var jQuery              = require('jquery');
global.jQuery           = require('jquery');

// Events

$(document).ready(function(){
    
    
    if($('.page-blog').length > 0) {

        var maxHeight = -1;

        if($(window).width() > 500) {
    
            $('.blog__single--block').each(function() {
                maxHeight = maxHeight > $(this).outerHeight(true) ? maxHeight : $(this).outerHeight(true);
            });
    
            $('.blog__single--block').each(function() {
                $(this).height(maxHeight + 35);
            });
            
        }

    }
    
});