Requirements:
- nodejs;
- npm;
- gulp;

1. Install nodejs with npm:
   - visit https://nodejs.org/en/,
   - download and install nodejs (LTS or Current version).

2. Install gulp:
   - npm install gulp-cli -g,
   - npm install gulp -D

3. Go to project folder and run "npm install".
4. Run "gulp watch" for watch process to start.